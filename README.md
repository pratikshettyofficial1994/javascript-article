# JAVASCRIPT DATA TYPES AND STRUCTURES


Data types are an integral part of a programming language, it is an attribute of the data which helps the computer to understand the context in which the programmer intends to use the data.
Each data type will have limited access of operations and methods. Hence each data type can be thought of as a class object having attributes and methods unique to it.

In Javascript, data types are of the following types:
1. Primitive
    * Undefined
    * Boolean
    * Number
    * String
    * BigInt
    * Symbol
2. Structured
    * Object
    * Array
    * Sets
    * Maps


## Primitive Type:

###   1. Undefined:

If a variable is not assigned any value, then javascript assigns it as 'undefined'.
It does not mean zero or null but the absence of any value stored in the variable.
```
>var a;
>conole.log(a)
undefined
```

###   2. Boolean:

Booleans represents logical or binary values and hence has two values: 'true' and 
'false'.
```
>var a = true;
>typeof(a); \\We can use typeof() to know the data type of a variable.
"boolean"
```

### 3. Null type:

Null in javascript is used to represent an intentional absence of object value in 
a variable.

```
>var foo = null;
>foo;
null
```

### 4. Number:

Number type is a 64-bit double-precision value ranging from '-(2^53 - 1)' to
'(2^53 - 1O)'. Apart from also representing floating-point values, it number type also represents three other values namely 'NaN(Not a Number)','+ Infinity' & '- Infinity'.

```
>var a = 5;
>typeof(a);
"Number"

>var b = 5.67;
>typeof(b);
"Number"

>var foo = 35/+0.0;
>typeof(foo);
"Infinity"

>var boo = 35/-0.0;
>typeof(boo);
"-Infinity"

```

### 5. BigInt:

BigInt type is used to store and operate on very large integers which are beyond the 
integer limit of number type. It is represented by appending n at the end of the integer.

```
> const x = 2n ** 53n;
9007199254740992n

> const y = x + 1n; 
9007199254740993n
```

### 6. String:

String type is used for representing textual data. The string type is stored as a set of 
"elements" of 16-bit unsigned integer values. Each element occupies a position in the string
and are indexed from '0'.

```
>const text = 'abcde';
>console.log(text[0]);
'a'

>lengthOfString = text.length;
>console.log(lengthOfString);
5
```

### 7. Symbol:

A Symbol type is a unique and immutable value. Every symbol value returned is a unique
object and are usually used as 'key' of an object property.

```
>let sym1 = Symbol('foo');
>let sym2 = Symbol('foo');

>Symbol('foo') === Symbol('foo');
false       \\because every Symbol is unique
```


## Structure Type:

### 1.Object:

In Javascript objects are a collection of properties. objects are stored as collections 
of key and value pairs where keys point towards it's respective values. The values can be
of any data type, including other objects or functions. And keys can have only Strings or
Symbol data types. Objects are unordered {key:value} pairs of data and every element can be accessed using its key. Objects are not iterable but we can use for-in loop.

```
>let employee = {name:'abc',
                salary:300000,
                printSalary:function(){
                            console.log(`My name is ${employee.name}
                                        and my salary is ${employee.salary}`)
                            }
                }
//access element

>employeeName = employee['name'];
abc
>employeeName = employee.name;
abc

//delete property

>delete employee.salary;

//for..in loop

>for (const property in employee) {
>  console.log(`${property}: ${employee[property]}`);
   }
"name: abc1"
"salary: 300000"
"printsalary: function(){
                        console.log(`My name is ${employee.name}
                                    and my salary is ${employee.salary}`)
                        }"
```

### 2. Arrays:

Arrays are list-like objects which can store values of any data type and the number of
elements that can be stored in an array is not fixed, it is dynamic and we can append
more elements in it. Array elements are ordered i.e the insertion order is maintained.
The elements from the list can be accessed via index. Arrays are also iterable 
and hence we can use for-of loop. It can also store duplicate values.

```
let a = ['abc', 1, 2.5, null];

//access elements
    
>a[0];
'abc'
>a[3];
2.5

//append elements

>a.push('hello');
>a;
['abc', 1, 2.5, null, 'hello']

//find element index
>a.findIndex(el ==> el === 'hello');
4

//delete element
>a.splice(0,1) //delete element at index 0
>console.log(a)
[1, 2.5, null, 'hello']

//for..of loop

>for (const ele of a){
>    conole.log(el)
>   }
1
2.5
null
'hello'
```

### 3. Sets:

Sets are similar to Arrays but the elements stored in sets are unordered and also we 
cannot store duplicate elements. Sets are iterable and hence we can use for-of loop
but the order may differ. In sets elements can't be accessed via index as the insertion
order is not maintained. Operations like finding elements are faster as compared to an 
array as the list is unordered. Also the size of the sets adjust dynamically.

```
>const id = new Set(1,2,3);

//add element

>id.add(4);
>console.log(id);
{1, 2, 3, 4}

>id.add(1);
>console.log(a);
{1, 2, 3, 4} //1 is not added as it already exists

//access elements
>console.log(id[2]);
undefined //cause sets are unordered

>console.log(id.has(1));
true

//delete element
>id.delete(1);
>console.log(id)
{2,3,4}

//for..of loop
>for (const el of id){
>    console.log(el);
>   }
1
2
3
4
```

### 4. Maps:

Map object holds a collection of {key:value} pairs and the insertion order is maintained
i.e it is an ordered list. Unlike object data structure here both the keys and values can
have any type of data type including objects. Maps are iterable and hence we can use the
for-of loop and returns an array of [key, value] for each iteration. Maps are meant for 
pure data storage and optimized for data access.

```
>const a = new Map();

//add element

>a.set('name','abc');
>a.set('age', 67);
>const country = {name:'India';Language:'Hindi'};
>a.set(country: 'greatest country');

//access all keys

>a.keys;
['name', 'age', country]

//access all values

>a.values;
['abc', 67, 'greatest country']

//access elements

>a.get('name');
'abc'

//delete elements

>a.delete('age');

//for..of loop

>for (const el in a){
    console.log(el);
>    }
['name', 'abc']
['age', 67]
[country, 'greatest country']
```

## References:

1. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures

